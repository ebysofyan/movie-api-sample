import random
import string


def string_generator(size=20, chars=string.ascii_letters + string.digits) -> str:
    return ''.join(random.choice(chars) for _ in range(size))
