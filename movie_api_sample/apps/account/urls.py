from django.urls import path

from .views import AccountActivationTemplateView

urlpatterns = [
    path('activation', AccountActivationTemplateView.as_view(), name='activation')
]
