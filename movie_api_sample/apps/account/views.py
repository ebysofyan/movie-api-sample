from typing import Any, Dict

from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from rest_framework.authtoken.models import Token

from .models import ActivationInformation


class AccountActivationTemplateView(TemplateView):
    template_name = 'account/account_activation.html'
    queryset = ActivationInformation.objects.all()

    def get_activation_info(self) -> ActivationInformation:
        return get_object_or_404(self.queryset, key=self.request.GET.get('key', None))

    def activate_account(self, user: User) -> None:
        user.is_active = True
        user.save()
        
        Token.objects.get_or_create(user=user)

    def do_activation(self) -> bool:
        activation_info: ActivationInformation = self.get_activation_info()
        activation_success = activation_info.is_valid
        activation_info.is_valid = False
        activation_info.save()
        if activation_success:
            self.activate_account(activation_info.user)
        return activation_success

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['activation_success'] = self.do_activation()
        return context
