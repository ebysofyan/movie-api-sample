from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils import timezone
from rest_framework import mixins, viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .serializers import UserLoginSerializer, UserRegisterSerializer


class UserRegisterViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (AllowAny, )
    serializer_class = UserRegisterSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request
        return context


class UserLoginViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (AllowAny, )
    serializer_class = UserLoginSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user: User = authenticate(
            request=request, **serializer.validated_data
        )
        if user:
            if not user.is_active:
                return Response({
                    'detail': 'User has not been activated, please contact admin'
                }, status=403)

            try:
                # login(request, user)
                user.last_login = timezone.now()
                user.save()
                return Response({
                    'token': user.auth_token.key
                })
            except User.auth_token.RelatedObjectDoesNotExist:
                return Response({
                    'detail': 'User has no Token, please contact provider to fix this issue'
                }, status=400)
        return Response({
            'detail': 'No user found with username and password combination'
        }, status=404)
