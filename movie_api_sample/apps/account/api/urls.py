from account.api.viewsets import UserLoginViewSet, UserRegisterViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter(trailing_slash=False)
router.register('register', UserRegisterViewSet, basename='register')
router.register('login', UserLoginViewSet, basename='login')

urlpatterns = []
urlpatterns += router.urls
