from account.models import ActivationInformation
from django.contrib.auth.models import User
from django.db import transaction
from django.urls import reverse_lazy
from rest_framework import serializers
from utils.string import string_generator


class ActivationInformationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivationInformation
        fields = ('key', 'url', )


class UserRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email')
        extra_kwargs = {
            'password': {
                'write_only': True
            },
            'email': {
                'required': False
            }
        }

    @property
    def request(self):
        return self.context.get('request')

    @transaction.atomic
    def create(self, validated_data):
        user = User.objects.create_user(**validated_data, is_active=False)
        activation_info, _ = ActivationInformation.objects.get_or_create(user=user, is_valid=True)
        activation_info.key = string_generator(size=50)
        activation_info.url = self.request.build_absolute_uri(
            reverse_lazy('account:activation') + '?key=%s' % activation_info.key
        )
        activation_info.save()
        return user

    def to_representation(self, instance: User):
        reps = super().to_representation(instance)
        activation_info = instance.activations.filter(is_valid=True).first()
        reps['activation_info'] = ActivationInformationSerializer(
            activation_info
        ).data
        return reps


class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
