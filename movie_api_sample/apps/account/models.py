from django.contrib.auth.models import User
from django.db import models
from django_extensions.db.models import TimeStampedModel


class ActivationInformation(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='activations')
    key = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    is_valid = models.BooleanField(default=False)
