from rest_framework import serializers

from .models import Movie


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = '__all__'
        extra_kwargs = {
            'owner': {
                'read_only': True
            }
        }

    def to_representation(self, instance: Movie):
        reps = super().to_representation(instance)
        reps['image'] = instance.image.url
        return reps
