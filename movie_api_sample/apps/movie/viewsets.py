
import cloudinary.uploader
from core.file import in_memory_file_to_temp
from core.pagination import CustomPageNumberPagination
from django.db import transaction
from django.db.models.query import QuerySet
from django_filters.rest_framework.backends import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated

from movie.models import Movie
from movie.serializers import MovieSerializer


class MovieViewSet(viewsets.ModelViewSet):
    serializer_class = MovieSerializer
    permission_classes = (IsAuthenticated, )
    queryset = Movie.objects.all()
    pagination_class = CustomPageNumberPagination
    filter_backends = (DjangoFilterBackend, SearchFilter,)
    filterset_fields = ('title', 'author', 'year', 'published')
    search_fields = ('title', 'author', 'year')

    def get_queryset(self) -> QuerySet:
        return super().get_queryset().filter(owner=self.request.user)

    @transaction.atomic
    def perform_create(self, serializer: MovieSerializer):
        instance = serializer.save()
        instance.owner = self.request.user
        instance.save()
