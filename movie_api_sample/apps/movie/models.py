from cloudinary import models as cloudinary_models
from django.contrib.auth.models import User
from django.db import models
from django_extensions.db.models import TimeStampedModel


class MovieCategory(TimeStampedModel):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)


class Movie(TimeStampedModel):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=100, blank=True)
    year = models.IntegerField(default=2021)
    genre = models.CharField(max_length=100)
    published = models.BooleanField(default=True)
    image = cloudinary_models.CloudinaryField('image')
    categories = models.ManyToManyField('MovieCategory', related_name='movies', blank=True)

    class Meta:
        ordering = ['created', 'title']
