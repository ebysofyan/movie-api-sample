from rest_framework.routers import DefaultRouter

from movie.viewsets import MovieViewSet

router = DefaultRouter(trailing_slash=False)
router.register('movies', MovieViewSet, basename='movies')

urlpatterns = []
urlpatterns += router.urls
