# Generated by Django 2.2.20 on 2021-08-02 14:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movie', '0004_auto_20210725_0540'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='categories',
            field=models.ManyToManyField(blank=True, related_name='movies', to='movie.MovieCategory'),
        ),
    ]
